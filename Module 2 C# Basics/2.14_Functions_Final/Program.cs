﻿int Fact(int n)
{
    int ans=1;
    for(int i=1;i<=n;i++)
    {
        ans*=i;
    }
    return ans;
}

//Recursive function
int Pow(int x,int y)
{
    if(y==0)
        return 1;
    else
        return x*Pow(x,y-1);
}

//Call by Ref
void swap(ref int x,ref int y)
{
    int temp=x;
    x=y;
    y=temp;
}

Console.WriteLine ($"Fact of 5 = {Fact(5)}");
Console.WriteLine($"5 Raise to 3 = {Pow(5,3)}");
int x=10,y=20;
swap(ref x,ref y);
Console.WriteLine($"X={x},Y={y}");


