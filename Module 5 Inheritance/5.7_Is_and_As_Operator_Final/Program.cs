﻿internal class Program
{
    public static void Main(string[] args)
    {
        Employee f=new FullTime(1,"John",12000);
        Console.WriteLine(f is FullTime);
        Console.WriteLine(f is Employee);
        Console.WriteLine(f is MyClass);
        Console.WriteLine (f is Object);
        FullTime ft= f as FullTime;
        ft.DisplayData();
    }
}