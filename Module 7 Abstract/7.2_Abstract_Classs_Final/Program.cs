﻿internal class Program
{
    public static void Main(string[] args)
    {
        Shape s;
        s = new Rectangle();
        (s as Rectangle).Width = 10;
        (s as Rectangle).Height = 20;
        Console.WriteLine($"Area of Rectangle :{s.Area()}");
        Console.WriteLine($"Perimeter of Rectangle :{s.Perimeter()}");
        s = new Circle();
        ((Circle)s).Radius = 10;
        Console.WriteLine($"Area of Circle :{s.Area()}");
        Console.WriteLine($"Perimeter of Circle :{s.Perimeter()}");
    }
}