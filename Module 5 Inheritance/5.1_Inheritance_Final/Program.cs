﻿internal class Program
{
    public static void Main(string[] args)
    {
        FullTime f = new FullTime();
        f.EmployeeNo = 1;
        f.EmployeeName = "Mahesh";
        f.Salary = 10000;
        Console.WriteLine($"No = {f.EmployeeNo} , Name = {f.EmployeeName} , Salary = {f.Salary} ");
        PartTime p = new PartTime();
        p.EmployeeNo = 2;
        p.EmployeeName = "Manoj";
        p.WorkingHour = 20;
        p.HourRate = 10;
        Console.WriteLine($"No = {p.EmployeeNo} , Name = {p.EmployeeName} , Pay = {p.WorkingHour * p.HourRate} ");


    }
}