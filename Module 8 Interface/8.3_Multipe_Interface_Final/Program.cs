﻿
internal class Program
{
    public static void Main(string[] args)
    {
        Rectangle r = new Rectangle();
        r.Height = 10;
        r.Width = 20;
        Console.WriteLine($"Area = {r.Area()}");
        Console.WriteLine($"Perimeter = {r.Perimeter()}");
        r.Show();
    }
}