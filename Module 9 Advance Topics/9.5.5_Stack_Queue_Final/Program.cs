﻿
internal class Program
{
    public static void Main(string[] args)
    {
        Stack<int> myStack = new Stack<int>();
        myStack.Push(1);
        myStack.Push(2);
        myStack.Push(3);
        myStack.Push(4);

        Console.Write("Number of elements in Stack: {0}\n", myStack.Count);
        foreach (var item in myStack)
            Console.WriteLine("item " + item);

        Console.WriteLine("Pop from Stack");
        while (myStack.Count > 2)
            Console.Write(myStack.Pop() + ",");

        Console.Write("\nNumber of elements in Stack: {0}\n", myStack.Count);
        foreach (var item in myStack)
            Console.WriteLine("item " + item);

        Queue<int> myQueue = new Queue<int>();
        myQueue.Enqueue(1);
        myQueue.Enqueue(2);
        myQueue.Enqueue(3);
        myQueue.Enqueue(4);

        Console.Write("Number of elements in Queue: {0}\n", myQueue.Count);
        foreach (var item in myQueue)
            Console.WriteLine("item " + item);

        Console.WriteLine("Dequeue from Queue");
        while (myQueue.Count > 2)
            Console.Write(myQueue.Dequeue() + ",");

        Console.Write("\nNumber of elements in Queue: {0}\n", myQueue.Count);
        foreach (var item in myQueue)
            Console.WriteLine("item " + item);
    }
}