﻿
internal class Program
{
    public static void Main(string[] args)
    {
        string fname = "Scott";
        string firstName = new string("Scott");
        string lastName = "Lee";
        Console.WriteLine(firstName.Equals(fname));
        Console.WriteLine(firstName.Equals(lastName));

        Console.WriteLine(firstName.CompareTo(fname));
        Console.WriteLine(firstName.CompareTo(lastName));
        Console.WriteLine(firstName.CompareTo("Scoty"));
        Console.WriteLine(firstName.CompareTo("Scot"));

        Console.WriteLine(firstName.StartsWith("Sc"));
        Console.WriteLine(firstName.StartsWith("Pc"));
        Console.WriteLine(firstName.Contains("co"));
        Console.WriteLine(firstName.Contains("ct"));
        Console.WriteLine(firstName.EndsWith("tt"));
        Console.WriteLine(firstName.EndsWith("z"));
    }
}