public class PartTime : Employee
{
    public int WorkingHour { get; set; }
    
    public double HourRate { get; set; }
}