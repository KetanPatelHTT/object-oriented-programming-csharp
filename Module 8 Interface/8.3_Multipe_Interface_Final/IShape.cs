﻿public interface IShape : IMyInter1, IMyInter2
{
    double Area();
    double Perimeter();
}
