﻿
using System.Collections;

internal class Program
{
    public static void Main(string[] args)
    {
        Hashtable numberNames = new Hashtable();
        numberNames.Add(1, "One"); //adding a key/value using the Add() method
        numberNames.Add(2, "Two");
        numberNames.Add(3, "Three");

        // The following throws run-time exception: key already added.
        //numberNames.Add(3,"");
        Console.WriteLine("\nNumber Table");
        foreach (DictionaryEntry de in numberNames)
            Console.WriteLine("Key: {0}, Value: {1}", de.Key, de.Value);

        //creating a Hashtable using collection-initializer syntax
        var cities = new Hashtable(){
                {"UK", "London, Manchester, Birmingham"},
                {"USA", "Chicago, New York, Washington"},
                {"India", "Mumbai, New Delhi, Pune"}
            };

        Console.WriteLine("\nCities Table");
        foreach (DictionaryEntry de in cities)
            Console.WriteLine("Key: {0}, Value: {1}", de.Key, de.Value);

        string citiesOfUK = (string)cities["UK"]; //cast to string
        string citiesOfUSA = (string)cities["USA"]; //cast to string

        Console.WriteLine("citiesOfUK  :" + citiesOfUK);
        Console.WriteLine("citiesOfUSA :" + citiesOfUSA);

        cities["UK"] = "Liverpool, Bristol"; // update value of UK key
        cities["USA"] = "Los Angeles, Boston"; // update value of USA key

        Console.WriteLine("\nCities Table After update");
        foreach (DictionaryEntry de in cities)
            Console.WriteLine("Key: {0}, Value: {1}", de.Key, de.Value);

        Console.WriteLine("Contains India:" + cities.Contains("India"));
        Console.WriteLine("Contains Mumbai, New Delhi, Pune:" + cities.ContainsValue("Mumbai, New Delhi, Pune"));
        if (!cities.ContainsKey("France"))
        {
            cities["France"] = "Paris";
        }

        Console.WriteLine("\nCities Table After adding France");
        foreach (DictionaryEntry de in cities)
            Console.WriteLine("Key: {0}, Value: {1}", de.Key, de.Value);

        cities.Remove("India");
        Console.WriteLine("\nCities Table After removing India");
        foreach (DictionaryEntry de in cities)
            Console.WriteLine("Key: {0}, Value: {1}", de.Key, de.Value);

    }
}