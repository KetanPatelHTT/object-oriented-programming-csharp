﻿public class Rectangle : Shape
{
     public double Height {get;set;}
     public double Width {get;set;}

    public override double Area()
    {
        return Height * Width;
    }

    public override double Perimeter()
    {
        return 2 * ( Height + Width);
    }

    public void Show()
    {
        Console.WriteLine("Rectangle Show");
    }
}
