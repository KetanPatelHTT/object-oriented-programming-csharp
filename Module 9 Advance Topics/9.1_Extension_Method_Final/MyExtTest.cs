﻿

static public class MyExtTest
{
     public static int Multiply(this MyTest myTest)
     {
          return myTest.val1 * myTest.val2;
     }

     public static String ConcatWithSpace(this String str, String strtail)
     {
          return str + " " + strtail;
     }

}
