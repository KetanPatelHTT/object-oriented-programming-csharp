﻿
internal class Program
{
    public static bool IsTeenAger(Student s)
    {
        if (s.Age >= 12 && s.Age < 18)
            return true;
        else
            return false;
    }
    public class Student
    {
        public int StudentID { get; set; }
        public string? StudentName { get; set; }
        public int Age { get; set; }
    }
    public static void Main(string[] args)
    {
        // Student collection
        IList<Student> studentList = new List<Student>() {
                new Student() { StudentID = 1, StudentName = "Dipak", Age = 13} ,
                new Student() { StudentID = 2, StudentName = "Rahul",  Age = 20 } ,
                new Student() { StudentID = 3, StudentName = "Jinal",  Age = 18 } ,
                new Student() { StudentID = 4, StudentName = "Dhruti" , Age = 20} ,
                new Student() { StudentID = 5, StudentName = "Mayank" , Age = 15 },
                new Student() { StudentID = 6, StudentName = "Dinky" , Age = 15 },
                new Student() { StudentID = 7, StudentName = "Bony" , Age = 12 },
                new Student() { StudentID = 8, StudentName = "Sunil" , Age = 15 }
            };
        var student = (from s in studentList
                       where s.Age == 13
                       select s).SingleOrDefault();
        if (student != null)
            Console.WriteLine($"Id : {student.StudentID} , Name : {student.StudentName} , Age {student.Age}");

        Console.WriteLine($"Max Age : {studentList.Max(s => s.Age)}");
        Console.WriteLine($"Min Age : {studentList.Min(s => s.Age)}");
        Console.WriteLine($"Avg Age : {studentList.Average(s => s.Age)}");
    }
}

