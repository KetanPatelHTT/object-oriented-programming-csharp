﻿internal class Program
{
    public static void Main(string[] args)
    {
        Employee e = new Employee(0, "");
        e.DisplayData();
        e = new FullTime(1, "Manish", 10000);
        e.DisplayData();
        e= new PartTime(2, "Manoj", 20, 1000);
        e.DisplayData();
    }
}