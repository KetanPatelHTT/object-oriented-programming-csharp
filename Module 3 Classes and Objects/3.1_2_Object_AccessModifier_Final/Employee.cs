public class Employee
{
    private int EmployeeNo;
    private string? EmployeeName;

    public void SetData(int empno, string? empname)
    {
        EmployeeNo = empno;
        EmployeeName = empname;
    }

    public virtual void DisplayEmployee()
    {
        Console.WriteLine($"No = {EmployeeNo} , Name = {EmployeeName} ");
    }
}


