public class Employee
{
    private int EmployeeNo;
    private string? EmployeeName;
    /*public Employee()
    {
        this.EmployeeNo = 0;
        this.EmployeeName = "";
    }*/
    public Employee() : this(0, "")
    { }
    public Employee(int EmployeeNo, string? EmployeeName)
    {
        this.EmployeeNo = EmployeeNo;
        this.EmployeeName = EmployeeName;
    }

    public Employee(Employee e) : this(e.EmployeeNo, e.EmployeeName)
    { }
    /*public Employee(Employee e)
    {
        this.EmployeeNo = e.EmployeeNo;
        this.EmployeeName = e.EmployeeName;
    }*/


    public void SetData(int EmployeeNo, string? EmployeeName)
    {
        this.EmployeeNo = EmployeeNo;
        this.EmployeeName = EmployeeName;
    }
    public void SetData()
    {
        this.EmployeeNo = 0;
        this.EmployeeName = "";
    }

    public virtual void DisplayEmployee()
    {
        Console.WriteLine($"No = {EmployeeNo} , Name = {EmployeeName} ");
    }
}

