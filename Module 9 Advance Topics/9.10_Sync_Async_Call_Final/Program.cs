﻿
internal class Program
{
    public static void SyncMethod()
    {
        // Simulate a delay of 3 seconds
        Thread.Sleep(3000);
        Console.WriteLine("Synchronous Call.");
    }

    // Simulating an asynchronous operation
    public static async Task AsyncMethod()
    {
        // Simulate a delay of 3 seconds asynchronously
        await Task.Delay(3000);
        Console.WriteLine("Asynchronous Call.");

    }
    public static void Main(string[] args)
    {
        Console.WriteLine("Starting synchronous operation...");
        SyncMethod();
        Console.WriteLine("Synchronous operation completed.");

        Console.WriteLine("\nStarting asynchronous operation...");
        Task task = AsyncMethod();
        Console.WriteLine("Asynchronous operation started.");
        task.Wait();
    }
}