﻿
internal class Program
{
    public static void Main(string[] args)
    {
        try
        {
           // String content = File.ReadAllText("FullTime.cs");
            double val1 = Double.Parse(Console.ReadLine());
            double val2 = Double.Parse(Console.ReadLine());
            Console.WriteLine($"Addition ={val1 + val2}");
        }
        catch (FileNotFoundException ex)
        {
            Console.WriteLine(ex.Message);
        }
        finally
        {
            Console.WriteLine("Execute Always");
        }
        Console.WriteLine("Next Statements");
    }
}