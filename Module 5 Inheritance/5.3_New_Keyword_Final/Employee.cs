public class Employee
{
    protected int EmployeeNo { get; set; }

    protected string? EmployeeName { get; set; }

    public void Set(int EmployeeNo, string? EmployeeName)
    {
        this.EmployeeNo = EmployeeNo;
        this.EmployeeName = EmployeeName;
    }
    public virtual void DisplayData()
    {
        Console.WriteLine($"No = {EmployeeNo} , Name = {EmployeeName} ");
    }
}