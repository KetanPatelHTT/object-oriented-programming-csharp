﻿
internal class Program
{
    public static void Main(string[] args)
    {
        string fname = "John", lname = "Lee";
        Console.WriteLine($" First Name={fname} , Last Name={lname}");

        char[] letters = { 'S', 'c', 'o', 't', 't' };
        //by using string constructor 
        string name = new string(letters);
        Console.WriteLine(name);

        string str1 = new string('K', 10);
        Console.WriteLine(str1);

        string str2 = new string(letters, 2, 3);
        Console.WriteLine(str2);

        for (int i = 0; i < fname.Length; i++)
        {
            Console.WriteLine(fname[i]);
        }
    }
}