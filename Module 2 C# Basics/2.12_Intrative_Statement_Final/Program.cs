﻿int n;
Console.Write("Enter N:");
n = int.Parse(Console.ReadLine() ?? "0");
int i = 1;
Console.WriteLine("While:");
while (i <= n)
{
    Console.WriteLine(i);
    i++;
}
Console.WriteLine("Do While:");
i = 1;
do
{
    Console.WriteLine(i);
    i++;
} while (i <= n);

Console.WriteLine("For:");
for (i = 1; i <= n; i++)
{
    Console.WriteLine(i);
}

int[] a = new int[] { 11, 22, 33 };
Console.WriteLine("Foreach:");
foreach (int val in a)
{
    Console.WriteLine(val);
}

