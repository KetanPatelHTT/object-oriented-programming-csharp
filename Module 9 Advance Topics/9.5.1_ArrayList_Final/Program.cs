﻿
using System.Collections;

internal class Program
{
    public static void Main(string[] args)
    {
        ArrayList list1 = new ArrayList();
        list1.Add(1);
        list1.Add("Bill");
        list1.Add(" ");
        list1.Add(true);
        list1.Add(4.5);
        list1.Add(null);
        // adding elements using object initializer syntax
        var list2 = new ArrayList()
        {
            2, "Steve", " ", true, 4.5, null
        };
        Console.WriteLine("\nFirst List");
        for (int i = 0; i < list1.Count; i++)
            Console.Write(list1[i] + ", ");

        Console.WriteLine("\nSecond List");
        foreach (var item in list2)
            Console.Write(item + ", ");


        list1.Insert(1, "Second Item");

        Console.WriteLine("\nFirst List After Insert Second Item");
        foreach (var val in list1)
            Console.Write(val + ", ");

        list1.InsertRange(2, list2);
        Console.WriteLine("\nFirst List After InsertRange");

        foreach (var item in list1)
            Console.Write(item + ", ");

        list1.Remove(null); //Removes first occurance of null
        Console.WriteLine("\nFirst List After Remove null");
        foreach (var item in list1)
            Console.Write(item + ", ");

        list1.RemoveAt(4); //Removes element at index 4
        Console.WriteLine("\nFirst List After Remove 4");
        foreach (var item in list1)
            Console.Write(item + ", ");

        list1.RemoveRange(0, 2);//Removes two elements starting from 1st item (0 index)

        Console.WriteLine("\nFirst List After Remove Range");
        foreach (var item in list1)
            Console.Write(item + ", ");

        Console.WriteLine("\nContains 2  :" + list1.Contains(2)); // true
        Console.WriteLine("Contains Bill :" + list1.Contains("Bill")); // true
        Console.WriteLine("Contains 10   :" + list1.Contains(10)); // false

    }
}