﻿internal class Program
{
    public static void Main(string[] args)
    {
        Console.WriteLine($"Total Employees  = {Employee.EmployeeCount} ");
        Employee e1;
        e1 = new Employee();
        e1.DisplayEmployee();

        Employee.DisplayCount();

        Employee e2 = new Employee(101, "Ram",GenderType.Female);
        e2.DisplayEmployee();

        Employee e3 = new Employee(102, "Man",GenderType.Other);
        e3.DisplayEmployee();

    }
}