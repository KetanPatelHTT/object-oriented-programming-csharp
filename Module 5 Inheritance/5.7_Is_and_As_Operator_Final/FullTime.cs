public class FullTime : Employee
{
    public double Salary { get; set; }

    public FullTime(int EmployeeNo, string? EmployeeName, double Salary) : base(EmployeeNo, EmployeeName)
    {
        this.Salary = Salary;
    }

    public void SetData(int EmployeeNo, string? EmployeeName, double Salary)
    {
        base.SetData(EmployeeNo, EmployeeName);
        this.Salary = Salary;
    }

    public override void DisplayData()
    {
        Console.WriteLine($"No = {EmployeeNo} , Name = {EmployeeName} , Salary = {Salary} ");
    }
}