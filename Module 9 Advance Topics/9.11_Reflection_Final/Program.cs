﻿
using System.Reflection;

internal class Program
{
    public static void Main(string[] args)
    {
        Assembly assembly = Assembly.GetExecutingAssembly();
        // Get all types in the assembly
        Type[] types = assembly.GetTypes();

        foreach (Type type in types)
        {
            Console.WriteLine($"Type: {type.FullName}");

            // Get all methods in the type
            MethodInfo[] methods = type.GetMethods();
            foreach (MethodInfo method in methods)
            {
                Console.WriteLine($"  Method: {method.Name}");
                ParameterInfo[] parameters = method.GetParameters();
                foreach (ParameterInfo parameter in parameters)
                {
                    Console.WriteLine($"    Parameter: {parameter.Name}, Type: {parameter.ParameterType}");
                }
            }

            // Get all properties in the type
            PropertyInfo[] properties = type.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                Console.WriteLine($"  Property: {property.Name}");
            }
        }
    }
}