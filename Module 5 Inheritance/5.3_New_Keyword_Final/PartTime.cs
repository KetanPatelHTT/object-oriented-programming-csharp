public class PartTime : Employee
{
    public int WorkingHour { get; set; }

    public double HourRate { get; set; }

    public void SetData(int EmployeeNo, string? EmployeeName, int WorkingHour, double HourRate)
    {
        this.EmployeeNo = EmployeeNo;
        this.EmployeeName = EmployeeName;
        this.WorkingHour = WorkingHour;
        this.HourRate = HourRate;
    }

    new public void DisplayData()
    {
        Console.WriteLine($"No = {EmployeeNo} , Name = {EmployeeName} , Pay = {WorkingHour * HourRate} ");
    }
}