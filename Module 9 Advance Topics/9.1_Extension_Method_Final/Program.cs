﻿
internal class Program
{
    public static void Main(string[] args)
    {
        MyTest myObj = new MyTest();
        myObj.val1 = 10;
        myObj.val2 = 20;
        Console.WriteLine($"Addition = {myObj.Addition()}");
        Console.WriteLine($"Multiply = {myObj.Multiply()}");
        String FirstName = "John";
        String LastName = "Lee";
        Console.WriteLine(FirstName.ConcatWithSpace(LastName));
    }
}