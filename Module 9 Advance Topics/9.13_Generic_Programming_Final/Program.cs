﻿
internal class Program
{
    class DataKeyValue<TKey, TValue>
    {
        public TKey Key { get; set; }
        public TValue Value { get; set; }

        public TValue GetValue(TKey Key)
        {
            if (this.Key.Equals(Key))
            {
                return Value;
            }
            else
            {
                return default(TValue);
            }
        }
    }

    public static void Main(string[] args)
    {
        DataKeyValue<int, string> kvp = new DataKeyValue<int, string>();
        kvp.Key = 1;
        kvp.Value = "Neel";
        Console.WriteLine(kvp.GetValue(1));

        DataKeyValue<string, string> kvp1 = new DataKeyValue<string, string>();
        kvp1.Key = "First";
        kvp1.Value = "Neel";
        Console.WriteLine(kvp1.GetValue("First"));
    }
}