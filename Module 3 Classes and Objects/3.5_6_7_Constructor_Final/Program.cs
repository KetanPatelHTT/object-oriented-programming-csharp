﻿internal class Program
{
    public static void Main(string[] args)
    {

        Employee e;        
        e = new Employee();             //default constructor
        e.DisplayEmployee();
        e = new Employee(10, "Ramesh"); //parameter constructor         
        e.DisplayEmployee();
        Employee ec=new Employee(e);    //copy constructor
        ec.DisplayEmployee();

    }
}