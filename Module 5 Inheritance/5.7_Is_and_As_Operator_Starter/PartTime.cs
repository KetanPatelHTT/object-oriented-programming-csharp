public class PartTime : Employee
{
    public int WorkingHour { get; set; }

    public double HourRate { get; set; }

    public PartTime(int EmployeeNo, string? EmployeeName, int WorkingHour, double HourRate) : base(EmployeeNo, EmployeeName)
    {
        this.WorkingHour = WorkingHour;
        this.HourRate = HourRate;
    }

    public void SetData(int EmployeeNo, string? EmployeeName, int WorkingHour, double HourRate)
    {
        this.EmployeeNo = EmployeeNo;
        this.EmployeeName = EmployeeName;
        this.WorkingHour = WorkingHour;
        this.HourRate = HourRate;
    }

    override public void DisplayData()
    {
        Console.WriteLine($"No = {EmployeeNo} , Name = {EmployeeName} , Pay = {WorkingHour * HourRate} ");
    }
}