﻿
internal class Program
{
    public static void Main(string[] args)
    {
        string firstName = new string("Scott");
        Console.WriteLine($"IndexOf t     :{firstName.IndexOf("t")}");
        Console.WriteLine($"LastIndexOf t :{firstName.LastIndexOf("t")}");
        Console.WriteLine($"IndexOf u     :{firstName.IndexOf("u")}");
        Console.WriteLine($"ToLower       :{firstName.ToLower()}");
        Console.WriteLine($"ToUpper       :{firstName.ToUpper()}");
        Console.WriteLine($"Insert Hello  :{firstName.Insert(0, "Hello ")}");
        Console.WriteLine($"Length        :{firstName.Length}");
        Console.WriteLine($"Remove  3,1   :{firstName.Remove(3, 1)}");
        Console.WriteLine($"Replace t i   :{firstName.Replace('t', 'i')}");
        Console.WriteLine($"Split String  :Kevin,John|Paul");
        string[] split = "Kevin,John|Paul".Split(new char[] { ',', '|', '.' });
        foreach (string str in split)
        {
            Console.WriteLine(str);
        }
        Console.WriteLine($"Substring 1,2 :{firstName.Substring(1, 2)}");
        Console.WriteLine($"Trim          :{"   Scott    ".Trim()}");
        Console.WriteLine($"PadLeft 10 t  :{firstName.PadLeft(10)}");
        Console.WriteLine($"PadRight 10 x :{firstName.PadRight(10, 'x')}");
    }
}