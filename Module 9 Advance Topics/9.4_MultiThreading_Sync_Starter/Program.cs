﻿
internal class Program
{
    public static void ThreadFunc()
    {
        for (int i = 1; i <= 15; i++)
        {
            Console.WriteLine($"Thread {Thread.CurrentThread.Name} , {i} ");
        }
    }
    public static void Main(string[] args)
    {
        Thread t1 = new Thread(new ThreadStart(ThreadFunc));
        Thread t2 = new Thread(new ThreadStart(ThreadFunc));
        t1.Name = "First";
        t2.Name = "Second";
        t1.Start();
        t2.Start();
        t1.Priority = ThreadPriority.Highest;
        t1.Join();
        t2.Join();
        Console.WriteLine("Main Thread Ends");
    }
}