﻿void SingleArrayTest()
{
    int[] a;
    int n;
    Console.WriteLine("Enter N : ");
    n = int.Parse(Console.ReadLine());
    a = new int[n];
    for (int i = 0; i < n; i++)
    {
        Console.Write("Enter [{0}] :", i);
        a[i] = int.Parse(Console.ReadLine());
    }
    for (int i = 0; i < n; i++)
    {
        Console.WriteLine($" [{i}] : {a[i]}");
    }
}
void MultiArrayTest()
{
    int[,] arr;
    int n;
    Console.WriteLine("Enter N : ");
    n = int.Parse(Console.ReadLine());
    arr = new int[n, n];
    Console.WriteLine("Pascal Triangle : ");
    for (int i = 0; i < n; i++)
    {
        for (int k = n; k >= i; k--)
        {
            Console.Write(" ");
        }

        for (int j = 0; j <= i; j++)
        {
            if (j == 0 || i == j)
            {
                arr[i, j] = 1;
            }
            else
            {
                arr[i, j] = arr[i - 1, j] + arr[i - 1, j - 1];
            }
            Console.Write(arr[i, j] + " ");
        }
        Console.WriteLine();

    }
}
void JArrayTest()
{
    int[][] jarr;
    int n;
    Console.WriteLine("Enter N : ");
    n = int.Parse(Console.ReadLine());
    Console.WriteLine("Pascal Triangle : ");
    jarr = new int[n][];
    for (int i = 0; i < n; i++)
    {
        jarr[i] = new int[i + 1];
        for (int k = n; k >= i; k--)
        {
            Console.Write(" ");
        }
        for (int j = 0; j <= i; j++)
        {
            if (j == 0 || i == j)
            {
                jarr[i][j] = 1;
            }
            else
            {
                jarr[i][j] = jarr[i - 1][j] + jarr[i - 1][j - 1];
            }
            Console.Write(jarr[i][j] + " ");
        }
        Console.WriteLine();
    }
}

SingleArrayTest();
MultiArrayTest();
JArrayTest();

