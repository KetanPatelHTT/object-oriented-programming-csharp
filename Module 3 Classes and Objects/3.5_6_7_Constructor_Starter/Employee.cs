public class Employee
{
    private int EmployeeNo;
    private string? EmployeeName;

    public void SetData(int EmployeeNo, string? EmployeeName)
    {
        this.EmployeeNo = EmployeeNo;
        this.EmployeeName = EmployeeName;
    }
    public void SetData()
    {
        this.EmployeeNo = 0;
        this.EmployeeName = "";
    }

    public virtual void DisplayEmployee()
    {
        Console.WriteLine($"No = {EmployeeNo} , Name = {EmployeeName} ");
    }
}

