﻿
public class Rectangle : IShape , IMyInter1 , IMyInter2 
{
    public double Height { get; set; }
    public double Width { get; set; }

    public double Area()
    {
        return Height * Width;
    }

    public double Perimeter()
    {
        return 2 * (Height + Width);
    }

    public void Show()
    {
        Console.WriteLine("Rectangle Show");
    }
}
