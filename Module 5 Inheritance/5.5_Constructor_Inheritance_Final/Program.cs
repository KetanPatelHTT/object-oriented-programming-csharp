﻿internal class Program
{
    public static void Main(string[] args)
    {
        Employee e = new Employee();
        e.SetData(0, "");
        e.DisplayData();
        e = new FullTime();
        ((FullTime)e).SetData(1, "Manish", 10000);
        e.DisplayData();
        e= new PartTime();
        ((PartTime)e).SetData(2, "Manoj", 20, 1000);
        e.DisplayData();
    }
}