public class FullTime : Employee
{
    public double Salary { get; set; }

    public void SetData(int EmployeeNo, string? EmployeeName, double Salary)
    {
        this.EmployeeNo = EmployeeNo;
        this.EmployeeName = EmployeeName;
        this.Salary = Salary;
    }

    public new void DisplayData()
    {
        Console.WriteLine($"No = {EmployeeNo} , Name = {EmployeeName} , Salary = {Salary} ");
    }
}