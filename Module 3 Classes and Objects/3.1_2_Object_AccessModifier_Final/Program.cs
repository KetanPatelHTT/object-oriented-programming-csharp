﻿
internal class Program
{
    public static void Main(string[] args)
    {
        Employee e;          // Reference Created
        e = new Employee();  // Object Created
        e.SetData(10, "Ramesh");
        e.DisplayEmployee();
    }
}