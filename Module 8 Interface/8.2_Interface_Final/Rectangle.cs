﻿
public class Rectangle : IShape
{
    public double Height { get; set; }
    public double Width { get; set; }

    public double Area()
    {
        return Height * Width;
    }

    public double Perimeter()
    {
        return 2 * (Height + Width);
    }

}
