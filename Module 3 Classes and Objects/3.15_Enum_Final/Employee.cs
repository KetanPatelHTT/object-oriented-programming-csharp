public enum GenderType
{
    Other=2,
    Male=0,
    Female=1,
}
public class Employee
{
    private int employee_no;
    private string? employee_name;

    public GenderType Gender { get; set; }

    public static int EmployeeCount;
    public readonly bool status;
    /*public Employee()
    {
        this.EmployeeNo = 0;
        this.EmployeeName = "";
    }*/
    public Employee() : this(0, "",0)
    { }
    public Employee(int EmployeeNo, string? EmployeeName,GenderType Gender)
    {
        this.EmployeeNo = EmployeeNo;
        this.EmployeeName = EmployeeName;
        this.Gender = Gender;
        status=true;
        EmployeeCount++;
    }

    public Employee(Employee e) : this(e.EmployeeNo, e.EmployeeName,e.Gender)
    { }
    /*public Employee(Employee e)
    {
        this.EmployeeNo = e.EmployeeNo;
        this.EmployeeName = e.EmployeeName;
    }*/


    public void SetData(int EmployeeNo, string? EmployeeName)
    {
        this.EmployeeNo = EmployeeNo;
        this.EmployeeName = EmployeeName;
    }
    public void SetData()
    {
        this.EmployeeNo = 0;
        this.EmployeeName = "";
    }

    public virtual void DisplayEmployee()
    {
        Console.WriteLine($"No = {EmployeeNo} , Name = {EmployeeName} ");
        Console.WriteLine($"Gender = {Gender}" );
        Console.WriteLine($"Total Employees  = {EmployeeCount} ");
    }

    public static void DisplayCount()
    {
        Console.WriteLine($"Static Total Employees  = {EmployeeCount} ");
    }

    public int EmployeeNo
    {
        get
        {
            return employee_no;
        }
        set
        {
            if (value < 0)
            {
                employee_no = value;
                Console.WriteLine("Invalid Employee No");
            }
            else
            {
                employee_no = value;
            }

        }
    }

    public string? EmployeeName
    {
        get
        {
            return employee_name;
        }
        set
        {
            employee_name = value;
        }
    }
    public string? EmpName
    {
        get
        {
            return employee_name;
        }

    }
    public string? EmplName
    {

        set
        {
            employee_name = value;
        }
    }
}