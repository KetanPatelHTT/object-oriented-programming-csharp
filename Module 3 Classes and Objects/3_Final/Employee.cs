public class Employee
{
    private int EmployeeNo { get; set; }

    private string? EmployeeName { get; set; }

    public Employee()
    {
        this.EmployeeNo = 0;
        this.EmployeeName = "";
    }
    public Employee(int EmployeeNo, string? EmployeeName)
    {
        this.EmployeeNo = EmployeeNo;
        this.EmployeeName = EmployeeName;
    }

    ~Employee()
    {
        Console.WriteLine("Destructor Called");
    }

    public void SetData(int EmployeeNo, string? EmployeeName)
    {
        this.EmployeeNo = EmployeeNo;
        this.EmployeeName = EmployeeName;
    }
    public void SetData()
    {
        this.EmployeeNo = 0;
        this.EmployeeName = "";
    }

    public virtual void DisplayEmployee()
    {
        Console.WriteLine($"No = {EmployeeNo} , Name = {EmployeeName} ");
    }
}

