﻿
int x = 10;
int y = 20;

string msg = "x= " + x.ToString() + " y=" + y.ToString();
Console.WriteLine(msg);
Console.WriteLine("x= " + x.ToString() + " y=" + y.ToString());
// Composite formatting:
Console.WriteLine("x= {0} y= {1}", x, y);
// String interpolation:
msg = $"x= {x} y= {y}";
Console.WriteLine($"x= {x} y= {y}");
Console.WriteLine(msg);

