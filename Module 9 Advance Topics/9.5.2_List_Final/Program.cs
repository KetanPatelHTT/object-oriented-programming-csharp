﻿using System.Collections;
internal class Program
{
    public static void Main(string[] args)
    {
        var cities = new List<string>();
        cities.Add("Delhi");
        cities.Add("Mumbai");
        cities.Add("Surat");
        cities.Add("Bhopal");

        Console.WriteLine("\n Indian Cities");
        foreach (var item in cities)
            Console.Write(item + ", ");
        Console.WriteLine();

        List<string> sCities = new List<string>()
                    {
                        "Vizag",
                        "Chennai",
                        "Banglore",
                    };
        Console.WriteLine("\n South Indian Cities");
        foreach (var item in sCities)
            Console.Write(item + ", ");
        Console.WriteLine();

        string[] rajCities = new string[2] { "Jaipur", "Jodhpur" };
        // adding an array in a List
        cities.AddRange(rajCities);
        Console.WriteLine("\n Indian Cities after adding range of Raj. Cities");
        foreach (var item in cities)
            Console.Write(item + ", ");
        Console.WriteLine();

        cities.Insert(1, "Ambala");
        Console.WriteLine("\n Indian Cities after insert Ambala at 1");
        foreach (var item in cities)
            Console.Write(item + ", ");
        Console.WriteLine();
    }
}