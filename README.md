### Course Material for my object oriented programming in c# Course
This branch of the repo contains starter projects and final code for all modules and projects of the course, exactly as shown in the videos.

Use starter project to start each section, and final code to compare it with your own code whenever something doesn't work!