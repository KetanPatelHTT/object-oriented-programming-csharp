﻿internal class Program
{
    public static void Main(string[] args)
    {
        Employee e= new Employee();  
        e.Set(0,"");
        e.DisplayData();
        FullTime f=new FullTime();
        f.SetData(1,"Manish",10000);
        f.DisplayData();       
        PartTime p = new PartTime();
        p.SetData(2,"Manoj",20,1000);
        p.DisplayData();
    }
}