﻿
internal class Program
{
    readonly static object lockobj=new object();
    public static void ThreadFunc()
    {
        lock(lockobj) {
            Console.WriteLine($" Resource Allocated {Thread.CurrentThread.Name} ");
            Thread.Sleep(1000);
            Console.WriteLine($" Resource Deallocated {Thread.CurrentThread.Name}");
        }
     }
    public static void Main(string[] args)
    {
        Thread t1 = new Thread(new ThreadStart(ThreadFunc));
        Thread t2 = new Thread(new ThreadStart(ThreadFunc));
        t1.Name = "First";
        t2.Name = "Second";
        t1.Start();
        t2.Start();
        t1.Priority = ThreadPriority.Highest;
        t1.Join();
        t2.Join();
        Console.WriteLine("Main Thread Ends");
    }
}