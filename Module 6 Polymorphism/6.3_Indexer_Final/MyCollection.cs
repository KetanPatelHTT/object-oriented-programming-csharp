﻿

public class MyCollection
{
    private string[] data;

    public MyCollection(int size)
    {
        data = new string[size];
    }

    public string this [int index]
    {
         get
         {
            if(index<0 || index>=data.Length )
                throw new IndexOutOfRangeException();
            else
                return data[index];    
         }
         set
         {
            if(index<0 || index>=data.Length )
                throw new IndexOutOfRangeException();
            else
                data[index]=value; 
         }
    }
    public void SetValue(int index,string value)
    {
        data[index]=value;
    }

    public string GetValue(int index)
    {
        return data[index];
    }

}
