﻿

public class MyCollection
{
    private string[] data;

    public MyCollection(int size)
    {
        data = new string[size];
    }

    public void SetValue(int index, string value)
    {
        data[index] = value;
    }

    public string GetValue(int index)
    {
        return data[index];
    }

}
