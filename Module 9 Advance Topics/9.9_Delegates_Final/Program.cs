﻿
internal class Program
{
    delegate int MaxMin(int n1, int n2);
    public static int Max(int n1, int n2)
    {
        return n1 > n2 ? n1 : n2;
    }
    public static int Min(int n1, int n2)
    {
        return n1 < n2 ? n1 : n2;
    }

    public static void Main(string[] args)
    {
        MaxMin MM = new MaxMin(Max);
        Console.WriteLine("Max is {0}", MM(10, 25));
        MM = new MaxMin(Min);
        Console.WriteLine("Min is {0}", MM(10, 25));
    }
}