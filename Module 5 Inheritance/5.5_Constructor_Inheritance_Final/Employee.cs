public class Employee
{
    protected int EmployeeNo { get; set; }

    protected string? EmployeeName { get; set; }

    public Employee()
    {
        Console.WriteLine("Employee Constructor");
    }

    public void SetData(int EmployeeNo, string? EmployeeName)
    {
        this.EmployeeNo = EmployeeNo;
        this.EmployeeName = EmployeeName;
    }
    public virtual void DisplayData()
    {
        Console.WriteLine($"No = {EmployeeNo} , Name = {EmployeeName} ");
    }
}