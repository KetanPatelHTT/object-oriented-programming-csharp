﻿internal class Program
{
    public static void Main(string[] args)
    {
        MyCollection collection = new MyCollection(3);

        collection.SetValue(0, "First");
        collection.SetValue(1, "Second");
        collection.SetValue(2, "Third");

        for (int i = 0; i < 3; i++)
        {
            Console.WriteLine($"Element at index {i}: {collection.GetValue(i)}");
        }
    }
}